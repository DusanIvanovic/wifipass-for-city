﻿<%@ Page Language="C#" AutoEventWireup="true"  MasterPageFile="~/Site.Master" CodeFile="UserLoggedIn.aspx.cs" Inherits="WiFIPass_for_CIty.UserLoggedIn" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <style>

    #EditNetwork, #addNewNetwork,#DUGME{
    width:300px;
    height:200px;
    margin-top:30px;
    text-align:center;
    padding-top:50px;
    }
    </style>
        
    <div class="container  text-center text-light">
                    <br />
                    <h5 id="welcome" runat="server"></h5>
                    <asp:Button ID="LogOut" runat="server" Text="Odjavi me" class="btn btn-md btn-dark" OnClick="LogOut_Click"  />
                    <br /><br />
        
                 <h1><b>WiFiPass for City</b></h1>
                 <h3>User Panel</h3>
                 <h5>Koristeći User Panel možete dodavati nove mreže ili menjati postojeće.</h5>
                 <br />
                 </div>
             </div>
     
    <div class="container k text-center text-light" style="height:500px;">
        
        <div class="container">
          
        <a href="./NewNetwork.aspx" class="btn btn-lg btn-success" id="addNewNetwork"><i class="fa fa-plus fa-4x"></i><br /><h2>Dodaj mrežu</h2></a>
         
        <a href="./UserDisplayNetworks.aspx" class="btn btn-lg btn-dark" id="EditNetwork"><i class="fa fa-wifi fa-4x"></i><br /><h2>Pregledaj mreže</h2></a>
        
       </div>
        
        
    
    
   
      
     
 </asp:Content>
