﻿using BusinessLayer;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WiFIPass_for_CIty
{
    public partial class UserDisplayNetworks : System.Web.UI.Page
    {
        private AccessPointBusiness accessPointBusiness;
        protected void Page_Load(object sender, EventArgs e)
        {
            this.accessPointBusiness = new AccessPointBusiness();

            
            String[] cities = new String[17] {"Beograd", "Novi Sad", "Kragujevac", "Niš", "Čačak", "Kraljevo", "Smederevo",
            "Užice", "Valjevo", "Subotica", "Šabac", "Pirot", "Kruševac", "Loznica", "Zaječar", "Kikinda", "Sombor"};

            for (int i = 0; i < cities.Length; i++)
            {
                listCity.Items.Add(cities[i].ToString());
            }


        }

       
        protected void search_Click(object sender, EventArgs e)
        {
            string selectedValue = listCity.SelectedValue.ToString();
            int count = 0, citiesCount = 0;
            SqlDataReader reader = this.accessPointBusiness.DisplayAccessPoint(selectedValue);
            LiteralControl deck = new LiteralControl();
            LiteralControl container = new LiteralControl();
            container.Text += "<div class" + "=" + "\"container\"" + ">";
            this.Controls.Add(container);
            deck.Text += "<div class=" + "\"card-deck\"" + ">";
            this.Controls.Add(deck);

            if (reader.HasRows == false)
            {
                message.Text = "Ne postoje rezultati za izabrani grad.";
                message.Attributes.Add("style", "border: 1px solid white");
                Response.AddHeader("REFRESH", "2,URL =UserDisplayNetworks.aspx");
            }



            while (reader.Read())
            {

                if (count == 3)
                {
                    count = 0;
                    LiteralControl command0 = new LiteralControl();
                    command0.Text += "</div>";
                    command0.Text += "<br/>";
                    command0.Text += "<div class=" + "\"card-deck\"" + ">";

                    this.Controls.Add(command0);
                }
                if (count <= 3)
                {
                    LiteralControl card = new LiteralControl();
                    string command1 = "<div class" + "=" + "\"col\"" + ">";
                    string command2 = "<div class" + "=" + "\"" + "card text-dark bg-light " + "\"" + "style" + "=" + "\"width: 18rem;\"" + ">";
                    string command3 = "<div class" + "=" + "\"card-body\"" + ">";
                    string command4 = "<h5 class" + "=" + "\"card-title\"" + ">" + "<b>"+ reader.GetString(0) + "</b>" +"</h5" + ">";
                    string command5 = "<p class" + "=" + "\"card-text\"" + ">" + reader.GetString(2) + "</p>";
                    string command6 = "<p class" + "=" + "\"card-text\"" + ">" + reader.GetString(3) + "</p>";
                    string command7 = "<p class" + "=" + "\"card-text\"" + ">" + reader.GetString(1) + "</p>" + "<hr/>";
                    string command8 = "<b>" + "Mreža:" + "</b>" + "<br>" + "<p class" + "=" + "\"card-text\"" + ">" + reader.GetString(4) + "</p>";
                    string command9 = "<b>" + "Šifra:" + "</b>" + "<br>" + "<p class" + "=" + "\"card-text\"" + ">" + reader.GetString(5) + "</p>";
                    string command10 = "</div>";
                    string command11 = "</div>";
                    string command12 = "</div>";

                    card.Text += command1;
                    card.Text += command2;
                    card.Text += command3;
                    card.Text += command4;
                    card.Text += command5;
                    card.Text += command6;
                    card.Text += command7;
                    card.Text += command8;
                    card.Text += command9;
                    card.Text += command10;
                    card.Text += command11;
                    card.Text += command12;
                   


                    this.Controls.Add(card);
                    count++;

                }

            }
            Literal containerEnd = new Literal();
            containerEnd.Text += "</div>";
            this.Controls.Add(containerEnd);

        }
        

    }
    
   

}


   

        
