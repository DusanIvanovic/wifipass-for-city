﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="AdminLoggedIn.aspx.cs" Inherits="WiFIPass_for_CIty.AdminLoggedIn" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <style>

   
    td{
        padding-top:10px;
        padding-bottom:10px;
        height:20px;
        border-bottom:1px solid white;
        
      
    }
    #info {
        background-color:lightgrey;
        color:black;
    }
     .listCity{
            font-size:25px;
            border:2px solid black;
            color:white;
            color:black;
            border-radius: 5px 5px;
            
        }
        .message{
            color:white;
            border-radius:5px 5px;
            padding:10px;
        }
        
    </style>
        
    <div class="container  text-center text-light">
                    <br />
                    <h5 id="welcome" runat="server"></h5>
                    <asp:Button ID="LogOut" runat="server" Text="Odjavi me" class="btn btn-md btn-dark" OnClick="LogOut_Click"  />
                    <br /><br />
        
                 <h1><b>WiFiPass for City</b></h1>
            </h3>
                 <h5>Koristeći Admin Panel možete dodavati nove mreže, menjati ili pregledati postojeće.</h5>
                 <br />
         <div class="container text-light text-center" >
           <h6>Iz padajuće liste izaberite grad za koji želite da vidite dostupne mreže.</h6>
           <asp:DropDownList id="listCity" runat="server" CssClass="listCity"></asp:DropDownList>
           <br/><br />
           <asp:Button id="search" runat="server" Text="Pretraži"  CssClass="btn btn-secondary btn-lg" OnClick="search_Click"/>
           <br /><br />
           <asp:Label id="message" runat="server" CssClass="message"></asp:Label>
                 
                
    <br/><br />

    
 
<asp:Repeater ID="rpt" runat="server" >
<HeaderTemplate>
    <table>
<tr style="background-color: white; color: black; font-weight: bold; height: 30px;">
        <th scope="col" style="width:200px;">
        Naziv objekta
        </th>
        <th scope="col" style="width:200px;">
        Adresa
        </th>
        <th scope="col" style="width:170px;">
        Grad
        </th>
        <th scope="col" style="width:100px;">
        Telefon
        </th>
        <th scope="col" style="width:150px;">
        Naziv mreže
        </th>
        <th scope="col" style="width:100px;">
        Šifra
        </th>
        <th scope="col" style="width:190px;">
        Akcija
        </th>
        </tr>
</HeaderTemplate>
<ItemTemplate>
        <tr>
             <td >

                 
                  <asp:Label ID="lblCompanyId" runat="server" Visible="false" Text='<%# Eval("CompanyId")%>' />
                  <asp:Label ID="lblCompanyName" runat="server" Text='<%# Eval("Name")%>' />
                 <asp:TextBox ID="txtCompanyName" runat="server" Width="200" Visible="false" Text='<%# Eval("Name")%>'/>
             </td>
             <td>
                 
                 <asp:Label ID="lblCompanyAddress" runat="server" Text='<%# Eval("Address")%>' />
                 <asp:TextBox ID="txtCompanyAddress" runat="server" Width="200" Visible="false" Text='<%# Eval("Address")%>' />
             </td>
              <td >
                  <asp:Label ID="lblCompanyCity" runat="server" Text='<%# Eval("City")%>' />
                 <asp:TextBox ID="txtCompanyCity" runat="server" Width="170" Visible="false" Text='<%# Eval("City")%>' />
              </td>
             <td >
                 <asp:Label ID="lblCompanyContact" runat="server" Text='<%# Eval("Contact")%>' />
                 <asp:TextBox ID="txtCompanyContact" runat="server" Width="100" Visible="false" Text='<%# Eval("Contact")%>' />
            </td>
            <td >
                <asp:Label ID="lblAccessPointId" runat="server" Visible="false" Text='<%# Eval("AccessPointId")%>' />
                <asp:Label ID="lblLocationId" runat="server" Visible="false" Text='<%# Eval("LocationId")%>' />
                <asp:Label ID="lblNetworkName" runat="server" Text='<%# Eval("NetworkName")%>' />
                 <asp:TextBox ID="txtNetworkName" runat="server" Width="150" Visible="false" Text='<%# Eval("NetworkName")%>' />
            </td>
            <td>
                <asp:Label ID="lblNetworkPassword" runat="server" Text='<%# Eval("Password")%>' />
                 <asp:TextBox ID="txtNetworkPassword" runat="server" Width="100" Visible="false" Text='<%# Eval("Password")%>' />
        
        </td>
        <td>
        <asp:Button ID="editRow" Text="Izmeni" runat="server" class="btn btn-warning btn-md" OnClick="OnEdit" />
        <asp:Button ID="updateRow" Text="Ažuriraj" runat="server" class="btn btn-dark btn-sm" Visible="false" OnClick="OnUpdate"   />
        <asp:Button ID="cancelRow" Text="Odustani" runat="server" class="btn btn-dark btn-sm" Visible="false" OnClick="OnCancel"/>
        <asp:Button ID="deleteRow" Text="Obriši" runat="server" class="btn btn-danger btn-md" OnClick="OnDelete" OnClientClick="return confirm('Da li stvarno želite obrišete red?');" />
        
        </td>
    </tr>
</ItemTemplate>
    <FooterTemplate>
        </table>
    </FooterTemplate>
</asp:Repeater>
     <table   id="info">
         <tr>
             <td style = "width: 200px">
                Naziv objekta:<br />
                 <asp:TextBox ID="txtCName" runat="server" Width='190' />
             </td>
              <td style = "width: 200px">
                Adresa:<br />
                 <asp:TextBox ID="txtCAddress" runat="server" Width='190' />
             </td>
              <td style = "width: 170px">
                Grad:<br />
                 <asp:TextBox ID="txtCCity" runat="server" Width='160' />
             </td>
              <td style = "width: 100px">
                Telefon:<br />
                 <asp:TextBox ID="txtCContact" runat="server" Width='90' />
             </td>
              <td style = "width: 150px">
                Naziv mreže:<br />
                 <asp:TextBox ID="txtNName" runat="server" Width='140' />
             </td>
              <td style = "width: 100px">
                Šifra:<br />
                 <asp:TextBox ID="txtNPassword" runat="server" Width='90' />
             </td>
              <td style = "width: 190px">
                
                  <asp:Button ID="addNetwork" Text="Dodaj" runat="server" Width='100' class="btn btn-success btn-lg" OnClick="OnInsert"  />
             </td>
         </tr>
     </table>
</table>
    </div>
   </div>
</asp:GridView>
     
   
        
        
    
    
   
      
     
                     
                
     
   
        
        
    
    
   
      
     
    </asp:Content>