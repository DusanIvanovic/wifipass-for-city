﻿using BusinessLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace WiFIPass_for_CIty
{
    public partial class About : Page
    {
        private UserBusiness userBussiness;


        protected void Page_Load(object sender, EventArgs e)
        {
            this.userBussiness = new UserBusiness();

        }

        protected void Login_Click(object sender, EventArgs e)
        {
            // Check if User exist in DB
           
            string username = Username.Value.ToString();
            string password = Password.Value.ToString();
            List<User> usersInDb = this.userBussiness.GetAllUsers();

            foreach (User u in usersInDb)
            {
                if (username.Equals(u.Username) && password.Equals(u.Password) && u.Permission.Equals("user"))
                {
                    //User redirect to the user page
                    UsernameWarning.InnerText = "Uspešna prijava!";
                    UsernameWarning.Style.Add("color", "green");
                    Session["Username"] = username;
                    HtmlMeta meta = new HtmlMeta();
                    meta.HttpEquiv = "Refresh";
                    meta.Content = "3;url=UserLoggedIn.aspx";
                    this.Page.Controls.Add(meta);
                   

                }
                else if (username.Equals(u.Username) && password.Equals(u.Password) && u.Permission.Equals("admin"))
                { 
                    //User redirect to the admin page
                    UsernameWarning.InnerText = "Uspešna prijava!";
                    UsernameWarning.Style.Add("color", "green");
                    Session["Username"] = username;
                    HtmlMeta meta = new HtmlMeta();
                    meta.HttpEquiv = "Refresh";
                    meta.Content = "3;url=AdminLoggedIn.aspx";
                    this.Page.Controls.Add(meta);
                }
                else if(username == "" || password == "")
                    {

                    UsernameWarning.InnerText = "Pogrešno korisničko ime ili lozinka!";
                   
                }   
            }
        }
    }
}