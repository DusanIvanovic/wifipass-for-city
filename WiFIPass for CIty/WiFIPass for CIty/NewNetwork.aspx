﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="NewNetwork.aspx.cs" Inherits="WiFIPass_for_CIty.NewNetwork" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        .listCity{
            font-size:20px;
        }
    </style>

    <div class="d-flex h-100 text-center">
               
           <div class="m-auto">
               <br /><br />
                <h2><b>Dodavanje mreže</b></h2><br /><br />

       <form>
     
              <div class="col">
                  
                  <label class="sr-only" for="CompanyName">CompanyName</label>
                    <div class="input-group">
                         <div class="input-group-prepend">
                             <div class="input-group-text"><i class="fa fa-home"></i></div>
                        </div>
                       
                 <input type="text" class="form-control" id="CompanyName" placeholder="Naziv objekta" runat="server"><br />
              </div>
                  <i class="fa fa-info-circle"></i><h6>Unesite stvarni naziv objekta. <br />Npr. <b>Mozzart Cafe</b></h6>
         </div><br />
            <div class="col">
                  <label class="sr-only" for="CompanyAdress">CompanyAdress</label>
                    <div class="input-group">
                         <div class="input-group-prepend">
                             <div class="input-group-text"><i class="fa fa-map-marker"></i></div>
                        </div>
                 <input  type="text" class="form-control" id="CompanyAddress" placeholder="Adresa objekta" runat="server">
              </div>
                <i class="fa fa-info-circle"></i><h6>Unesite adresu na kojoj se objekat nalazi. <br />Npr. <b>Gornji Milanovac, Takovska 10</b></h6>
         </div><br />
           <div class="col">
                  <label class="sr-only" for="CompanyAdress">CompanyCity</label>
                    <div class="input-group">
                         <div class="input-group-prepend">
                             <div class="input-group-text"><i class="fa fa-city"></i></div>
                        </div>
                 <input  type="text" class="form-control" id="CompanyCity" placeholder="Grad" runat="server">
              </div>
                <i class="fa fa-info-circle"></i><h6>Unesite grad u kojem se objekat  nalazi.</h6>
               <h6>Koristite slova bez kvačice.</h6>
               Npr. <b>Arandjelovac umesto Aranđelovac, <br />Cacak umesto Čačak</b></h6>
         </div><br />
             <div class="col">
                  <label class="sr-only" for="Phone">Phone</label>
                    <div class="input-group">
                         <div class="input-group-prepend">
                             <div class="input-group-text"><i class="fa fa-phone"></i></div>
                        </div>
                 <input type="text" class="form-control" id="CompanyPhone" placeholder="Kontakt telefon" runat="server">
                        </div>
                 <i class="fa fa-info-circle"></i><h6>Unesite telefon ako Vam je poznat. <br />Npr. <b>032/715-887</b></h6>
              </div><br />
           
           <div class="col">
                  <label class="sr-only" for="NetworkName">NetworkName</label>
                    <div class="input-group">
                         <div class="input-group-prepend">
                             <div class="input-group-text"><i class="fa fa-wifi"></i></div>
                        </div>
                 <input type="text" class="form-control" id="NetworkName" placeholder="Naziv mreže" runat="server">
                        </div>
                 <i class="fa fa-info-circle"></i><h6>Unesite tačan naziv WIFI mreže. <br />Npr. <b>mozzart2020</b></h6>
              </div><br />
           <div class="col">
                  <label class="sr-only" for="NetworkPassword">NetworkPassword</label>
                    <div class="input-group">
                         <div class="input-group-prepend">
                             <div class="input-group-text"><i class="fa fa-unlock"></i></div>
                        </div>
                 <input type="text" class="form-control" id="NetworkPassword" placeholder="Šifra WiFi mreže" runat="server">
                        </div>
                 <i class="fa fa-info-circle"></i><h6>Unesite ispravnu šifru mreže. <br />Šifre moraju biti identične. <br />Npr. <b>mozzart2020gm</b></h6>
              </div><br />

                <!-- Save network Button-->
                 <button class="btn btn-lg btn-success" id="SaveNetwork" role="button" runat="server" onserverclick="SaveNetworkUser"><i class="fa fa-save fa-2x"></i><br /><h2>Sčuvaj mrežu</h2></button>
                 <br /><br />
                 <a href="./UserLoggedIn.aspx" style="color:white;"><h3><u>Vrati se na početnu stranu</u></a></h3>
                
            
                 
                 
         </div>
           
    </form>



    </asp:Content>
