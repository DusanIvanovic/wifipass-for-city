﻿<%@ Page Title="Prijava" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="WiFIPass_for_CIty.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <link href="css/style.css" rel="stylesheet" />
    
    <style>
        #username, #password, #email {
    background-color: white;
    height: 50px;
    color: black;
    margin: auto;
}
.m-auto {
    padding: 20px;
    background-color: white;
    color: black;
    border-radius: 5%;
}

.col {
    align-content: center;
}

a {
    color: dodgerblue;
}

    a:hover {
        color: grey;
    }
    </style>
        <div id="showcase" class="d-flex justify-content-center align-items-center text-white"
        style="background-image: url(./images/pozadina.jpg);background-position: center bottom; min-height:900px;">
         
        <div class="container text-center">
           
        
           
        
       <div class="d-flex h-100 text-center" style="margin-top:-200px;">
        
       
           <div class="m-auto">
                <h1><b>Prijava</b></h1><br /><br />
       <form>
     
              <div class="col">
                  <label class="sr-only" for="Username">Username</label>
                    <div class="input-group">
                         <div class="input-group-prepend">
                             <div class="input-group-text"><i class="fa fa-user"></i></div>
                        </div>
                 <input type="text" class="form-control" id="Username" placeholder="Korisničko ime" runat="server">
              </div>
         </div><br />
            <div class="col">
                  <label class="sr-only" for="Password">Password</label>
                    <div class="input-group">
                         <div class="input-group-prepend">
                             <div class="input-group-text"><i class="fa fa-unlock"></i></div>
                        </div>
                 <input type="password" class="form-control" id="Password" placeholder="Lozinka" runat="server">
              </div>
         </div><br />
             
                
                 <!-- ASP.NET button. When the user press the button the data about new user will be sent to database-->
                 <asp:Button ID="Login" runat="server" Text="Prijavi me" class="btn btn-lg btn-secondary" OnClick ="Login_Click" /><br /><br />
                        <div id="message-box">
                             <h6 id="UsernameWarning" runat="server" style="color:red; background-color:white;"></h6>
                       </div> <br />
                 <a href="./Default.aspx"><u>Vrati se na početnu stranu</u></a>
            
           
         </div>
           
 </form>
    
           
           
    </div>
          
  </div>
        
  </div>
</asp:Content>
