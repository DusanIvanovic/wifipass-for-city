﻿using BusinessLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WiFIPass_for_CIty
{
    public partial class NewNetwork : System.Web.UI.Page
    {
        private CompanyBusiness companyBusiness;
        private AccessPointBusiness accessPointBusiness;
        private LocationBusiness locationBusiness;
        protected void Page_Load(object sender, EventArgs e)
        {

            this.companyBusiness = new CompanyBusiness();
            this.accessPointBusiness = new AccessPointBusiness();
            this.locationBusiness = new LocationBusiness();

        }

        protected void SaveNetworkUser(object sender, EventArgs e)
        {
            

            //Dodavanje nove kompanije u bazu na osnovu podataka sa forme
            Company c = new Company();
            c.Name = CompanyName.Value;
            c.Contact = CompanyPhone.Value;
            this.companyBusiness.insertCompany(c);

            //Dodavanje nove lokacije u bazu na osnovu podataka sa forme
            Location l = new Location();
            l.Address = CompanyAddress.Value;
            l.City = CompanyCity.Value;
            foreach (Company co in companyBusiness.GetAllCompanies())
            {
                if (c.Name == co.Name)
                {
                    c.Id = co.Id;
                }
            }
            l.Company_Id = c.Id;
            this.locationBusiness.insertLocation(l);
            AccessPoint a = new AccessPoint();
            a.Name = NetworkName.Value;
            a.Password = NetworkPassword.Value;

            foreach (Location lo in locationBusiness.GetAllLocations())
            {
                if (l.Address == lo.Address)
                {
                    l.Id = lo.Id;
                }
            }
            a.Location_Id = l.Id;
            this.accessPointBusiness.insertAccessPoint(a);
            Response.AddHeader("REFRESH", "2,URL=./UserLoggedIn.aspx");
        }
    }
}