﻿using BusinessLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace WiFIPass_for_CIty
{
    public partial class AdminLoggedIn : System.Web.UI.Page
    {
        private AccessPointBusiness accessPointBusiness;
        private CompanyBusiness companyBusiness;
        private LocationBusiness locationBusiness;
        protected void Page_Load(object sender, EventArgs e)
        {
            this.accessPointBusiness = new AccessPointBusiness();
            this.companyBusiness = new CompanyBusiness();
            this.locationBusiness = new LocationBusiness();

            if (Session["Username"] != null)
            {
                welcome.InnerText = " Dobrodošli, " + Session["Username"].ToString();

            }
            fillCities();

        }
        protected void LogOut_Click(object sender, EventArgs e)
        {
            //Kada korisnik klikne na dugme za odjavu, sesija se zatvara i vrsi se preusmeravanje na pocetnu Login stranu
            Session.Clear();
            Response.Redirect("./Login.aspx");
        }

        public void fillCities()
        {


            String[] cities = new String[18] {"Izaberi grad", "Beograd", "Novi Sad", "Kragujevac", "Niš", "Čačak", "Kraljevo", "Smederevo",
            "Užice", "Valjevo", "Subotica", "Šabac", "Pirot", "Kruševac", "Loznica", "Zaječar", "Kikinda", "Sombor"};


            for (int i = 0; i < cities.Length; i++)
            {

                listCity.Items.Add(cities[i].ToString());
            }
            ListItem item = listCity.Items.FindByValue("Izaberi grad");
            item.Attributes.Add("style", "color:grey");


        }



        protected void search_Click(object sender, EventArgs e)
        {
            /*Ucitava se vrednost onoga sto je korisnik izabrao u DropDown listi(ta vrednost je grad) i prosledjuje se funkciji koja prikazuje AccessPointe
            na osnovu prosledjenog grada */
            
            string selectedValue = listCity.SelectedValue.ToString();
            DataTable table = new DataTable();
            SqlDataReader reader = this.accessPointBusiness.DisplayAccessPoint(selectedValue);


            /*Ako upit nije vratio nista, odnosno redaer je prazan to znaci da ne postoji nijedan za pis u bazi za izabrani grad, korisnik se obavestava porukom
            i ponovo se ucitava stranica nakon 2 sekunde cekanja, kako vi korisnik opet izabrao grad i pokusao pretragu*/
            
            if (reader.HasRows == false)
            {
                message.Text = "Ne postoje rezultati za izabrani grad.";
                message.Attributes.Add("style", "border: 1px solid white");
                Response.AddHeader("REFRESH", "2,URL =AdminLoggedIn.aspx");

            }
            else
            {

            /* Ako je upit vratio rezultate, znaci da u readeru postoje podaci. Taj reader bice izvor odakle ce Repeater vuci podatke za prikaz. U Repeateru je definisan
            sablon za prikazivanje podataka u vidu HTML tabele. Podaci se prikazuju u vidu labela koje su smestene u vrste i kolone, a podaci se iz readera kupe po
            nezivima kolona koje se u njemu nalaze. Da bi se izbegao problem kada se javljaju dve kolone sa istim imenom, u SQL upitu za izlistavanje AccessPointa
            definisani su alijasi kolona.* Funkcija DataBind() upisuje podatke u odgovvarajuce kontrole na stranici.*/
            
                rpt.DataSource = reader;
                rpt.DataBind();
            }

            // Da se ne desi da lista bude dodana na listu, pa se onada pojavljuju dupli gradovi u listi
            listCity.Items.Clear();
            fillCities();

        }
        protected void OnEdit(object sender, EventArgs e)
        {
            RepeaterItem item = (sender as Button).Parent as RepeaterItem;
            this.ToggleElements(item, true);
        }

        private void ToggleElements(RepeaterItem item, bool isEdit)
        {
            /* Funkcija i navedene opcije koriste se kada korisnik zeli da izmeni neki red u tabeli ili zeli da tu akciju prekine. 
               Funkcije se dakle poziva kada se desi OnEdit() ili OnCancel() dogadjaj. Kada se desi OnEdit() dogadjaj poziva se ova funkcija i prosledjuje joj se
               true parametar kako bi se skriveni text-boxovi i dva dugmeta Izmeni i Odustani prikazali, a ostali elementi sakrili, a kada se desi OnCancel()
               dogadjaj prosledjuje se vrednost false, kako bi se sve vratilo na pocetno stanje. Odnsono element koji je podesen da ima vrednost isEdit, kada dobije 
               vrednost true postace vidljiva, tj. oni koji imaju vrednost !isEdit tj. negaciju na vrednost true  postace neviljivi jer ce negacija od !true biti false. 
               Isto sve vazi i kada se prosledi false, ali je sve obrnuto.*/
               
            item.FindControl("editRow").Visible = !isEdit;
            item.FindControl("updateRow").Visible = isEdit;
            item.FindControl("cancelRow").Visible = isEdit;
            item.FindControl("deleteRow").Visible = !isEdit;

            item.FindControl("lblCompanyName").Visible = !isEdit;
            item.FindControl("lblCompanyAddress").Visible = !isEdit;
            item.FindControl("lblCompanyCity").Visible = !isEdit;
            item.FindControl("lblCompanyContact").Visible = !isEdit;
            item.FindControl("lblNetworkName").Visible = !isEdit;
            item.FindControl("lblNetworkPassword").Visible = !isEdit;

            item.FindControl("txtCompanyName").Visible = isEdit;
            item.FindControl("txtCompanyAddress").Visible = isEdit;
            item.FindControl("txtCompanyCity").Visible = isEdit;
            item.FindControl("txtCompanyContact").Visible = isEdit;
            item.FindControl("txtNetworkName").Visible = isEdit;
            item.FindControl("txtNetworkPassword").Visible = isEdit;
        }
        protected void OnCancel(object sender, EventArgs e)
        {
            RepeaterItem item = (sender as Button).Parent as RepeaterItem;
            this.ToggleElements(item, false);

        }
        protected void OnUpdate(object sender, EventArgs e)
        {
            RepeaterItem item = (sender as Button).Parent as RepeaterItem;
            int companyId = int.Parse((item.FindControl("lblCompanyId") as Label).Text);
            int locationId = int.Parse((item.FindControl("lblLocationId") as Label).Text);
            int accessPointId = int.Parse((item.FindControl("lblAccessPointId") as Label).Text);


            //Update kompanije - kupe se podaci iz textBox-ova na formi koji su sakriveni, a vidljivi kada se stisne dugme Izmeni
            Company c = new Company();
            c.Id = companyId;
            c.Name = (item.FindControl("txtCompanyName") as TextBox).Text.Trim();
            c.Contact = (item.FindControl("txtCompanyContact") as TextBox).Text.Trim();
            this.companyBusiness.UpdateCompany(c);

            //Update lokacije - kupe se podaci iz textBox-ova na formi koji su sakriveni, a vidljivi kada se stisne dugme Izmeni
            Location l = new Location();
            l.Id = locationId;
            l.Address = (item.FindControl("txtCompanyAddress") as TextBox).Text.Trim();
            l.City = (item.FindControl("txtCompanyCity") as TextBox).Text.Trim();
            this.locationBusiness.UpdateLocation(l);

            //Update AccessPointa - kupe se podaci iz textBox-ova na formi koji su sakriveni, a vidljivi kada se stisne dugme Izmeni
            AccessPoint a = new AccessPoint();
            a.Id = accessPointId;
            a.Name = (item.FindControl("txtNetworkName") as TextBox).Text.Trim();
            a.Password = (item.FindControl("txtNetworkPassword") as TextBox).Text.Trim();
            this.accessPointBusiness.UpdateAcessPoint(a);

            Response.Redirect("./AdminLoggedIn.aspx");
        }
        protected void OnInsert(object sender, EventArgs e)
        {
            //Dodavanje nove kompanije u bazu na osnovu podataka sa forme
            Company c = new Company();
            c.Name = txtCName.Text;
            c.Contact = txtCContact.Text;
            this.companyBusiness.insertCompany(c);

            //Dodavanje nove lokacije u bazu na osnovu podataka sa forme
            Location l = new Location();
            l.Address = txtCAddress.Text;
            l.City = txtCCity.Text;
            foreach (Company co in companyBusiness.GetAllCompanies())
            {
                if (c.Name == co.Name)
                {
                    c.Id = co.Id;
                }
            }
            l.Company_Id = c.Id;
            this.locationBusiness.insertLocation(l);
            AccessPoint a = new AccessPoint();
            a.Name = txtNName.Text;
            a.Password = txtNPassword.Text;

            foreach (Location lo in locationBusiness.GetAllLocations())
            {
                if (l.Address == lo.Address)
                {
                    l.Id = lo.Id;
                }
            }
            a.Location_Id = l.Id;
            this.accessPointBusiness.insertAccessPoint(a);
            Response.Redirect("./AdminLoggedIn.aspx");

        }

        protected void OnDelete(object sender, EventArgs e)
        {
            RepeaterItem item = (sender as Button).Parent as RepeaterItem;
            int companyId = int.Parse((item.FindControl("lblCompanyId") as Label).Text);
            int locationId = int.Parse((item.FindControl("lblLocationId") as Label).Text);
            int accessPointId = int.Parse((item.FindControl("lblAccessPointid") as Label).Text);

            this.accessPointBusiness.DeleteAccessPoint(accessPointId);
            this.locationBusiness.DeleteLocation(locationId);
            this.companyBusiness.DeleteCompany(companyId);

            
            Response.Redirect("./AdminLoggedIn.aspx");
        }
    }
}