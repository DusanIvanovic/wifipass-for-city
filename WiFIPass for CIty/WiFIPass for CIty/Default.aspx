﻿<%@ Page Title="Početna" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WiFIPass_for_CIty._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

     
        <!-- Navigacioni meni -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-transparent position-absolute w-100">
        <a class="navbar-brand" href="#"><strong><h3>WiFi Pass for City</h3></strong></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
            aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="./Default.aspx">Početna</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#about-service">O servisu</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#question">Najčešća pitanja</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#contact">Kontakt</a>
                </li>
                <li class="nav-item">
                   <a class="btn btn-light btn-lg" href="./Login.aspx" role="button" id="login">Prijavi se</a>
                </li>
            </ul>
        </div>
    </nav>
    
    
       <div id="showcase" class="d-flex justify-content-center align-items-center text-white"
    style="background-image: url(./images/pozadina.jpg);background-position: center bottom; min-height:900px;">
    <div class="text-center" id="heading-content">
        <h2>Najveća baza WiFi lokacija sa pristupnim šiframa.</h2>
        <h1 class="display-1 font-weight-bold">
            WiFi Pass for City
        </h1>

        <p><h3>Registruj se odmah ako želiš da doprineseš da naša baza bude još veća, <br>ili brzo pronađi WiFi mrežu u tvojoj blizini.</h3></p>

        <a href="./Registration.aspx" class="btn btn-primary p-3 px-5" style="border-radius: 3rem;" id="registration" href="./Registration.aspx">
            <h3 class="m-0 text-white">REGISTRUJ SE</h3>
        </a>
        
    </div>
</div>

        
    <!-- Information about our WiFi service -->
    <br><br>
    <div class="container text-center" id="about-service">
    <h1 style="text-align: center;">Šta je to <b>WiFi Pass for City</b>?</h1><br>
    <p><h3><strong>WiFi Pass for City</strong> je online servis koji omogućava da na jednostavan i brz način pretražite bazu WiFi mreža i saznate šifru za svaku od njih.
        Dovoljno je da izaberete grad i dobićete sve raspoložive WiFi mreže za taj grad koje je neko uneo, kao i pristupne šifre. </h3></p>
    <div class="row align-items-center text-center align-items-center" id="row-service-info">
        <div class="col-md-4">
        <img src="./images/magnifying-glass.png" class="img-fluid">
        <p><h4>Jednostavan pretraga mreža. Samo izaberite grad i dobićete sve mreže koje se nalaze u tom gradu.</h4></p>
    </div>
    <div class="col-md-4">
        <img src="./images/money-bag.png" class="img-fluid">
        <p><h4>Servis je potpuno besplatan za korišćenje, bez skrivenih troškova.</h4></p>
    </div>
    <div class="col-md-4">
        <img src="./images/add-file.png" class="img-fluid">
        <p><h4>Jednostavno dodavanje novih mreža uz prethodnu registraciju</h4></p>
    </div>   
    </div>
    </div>
        <br><br><br>
    <div class="container" id="question">
    <h1>Najčešće postavljana pitanja</h1>
    </div>
        <br><br>  
<div class="container text-left text-white">
    <h2><i class="fa fa-question-circle"></i> Šta je WiFi Pass for City?</h2>
        <p class="question"><h5>WiFi Pass for City je online servis koji omogućava da na jednostavan i brz način pretražite bazu WiFi mreža i saznate šifru za svaku od njih.</h5></p>
    <h2><i class="fa fa-question-circle"></i> Da li se korišćenje servisa plaća?</h2>
        <p class="question"><h5>Korišćenje servisa je potpuno besplatni, bez bilo kakvih skrivenih troškova.</h5></p>
    <h2><i class="fa fa-question-circle"></i> Da li je potrebna registracija?</h2>
        <p class="question"><h5>DA. U slučaju da želite da dodajete nove mreže ili da pogledate postojeće, morate da se registrujete. <a href="./Registration.aspx" style="color:white;"><u>Registracija</u></a> je jednostavna i ne traje dugo.</h5></p>
    <h2><i class="fa fa-question-circle"></i> Moram li da budem konektovan/a na internet?</h2>
        <p class="question"><h5>DA. Morate biti konektovani na internet da bi koristili servis.</h5></p>
    <h2><i class="fa fa-question-circle"></i> Kako mogu da pretražim mreže u mom gradu?</h2>
        <p class="question"><h5>Prvo se prijavite na svoj nalog. Otvoriće se početna stranica na kojoj birate između dve opcije. Izaberite opciju  <b>Pogledaj mreže</b>. Otvoriće se stranica za pretragu, a vi samo treba da izaberete grad i kliknete na dugme <b>Pretraži<b/>. </h5></p>
    <h2><i class="fa fa-question-circle"></i> Kako mogu da dodam novu mrežu?</h2>
        <p class="question"><h5>Prijavite se na Vaš nalog. Iz ponuđenih opcija izaberite <b>Dodaj mrežu</b>.  Popunite tražena polja, a zatim pritisnite dugme <b>Sačuvaj</b>.</h5></p>
    <hr style=" border: 2px solid white; ">
</div>
   </div> 

        <!-- Contact information -->
        <div class="container text-center" id="contact" style="padding-top:100px; padding-bottom:50px;">
             <h1><strong>Kontakt podaci</strong></h1><br><br><br>
        <div class="row">
            <div class="col-md-4">
                <i class="fa fa-at fa-5x"></i><h2>sddateam@office.rs</h2>
            </div>
            <div class="col-md-4">
                <i class="fa fa-phone-square fa-5x"></i><h2>+381 32 714-885</h2>
            </div>
            <div class="col-md-4">
                <i class="fa fa-skype fa-5x"></i><h2>sddasrb</h2>
           </div>
    </div>
</div>
    <div class="footer" style=" background:url(./images/footer.jpg); padding:20px; text-align:center; margin-bottom:-50px; font-weight:normal;">
        <p>Copyright ©2019 SDDA. All rights reserved.</p> 
         </div>   
         
</asp:Content>
