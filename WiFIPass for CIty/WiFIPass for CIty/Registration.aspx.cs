﻿using BusinessLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WiFIPass_for_CIty
{
    public partial class Contact : Page
    {
        private UserBusiness userBussiness;


        protected void Page_Load(object sender, EventArgs e)
        {
            this.userBussiness = new UserBusiness();

        }


        protected void Registration_Click(object sender, EventArgs e)
        {

            List<User> usersInDB = this.userBussiness.GetAllUsers();


            bool toRedirect = false;
            int br = 0;
            string username = UsernameReg.Value.ToString();
            string password = PasswordReg.Value.ToString();
            string email = EmailReg.Value.ToString();



            foreach (User us in usersInDB)
            {


                if (email == us.Email || username == us.Username || password == "" || username == "" || email == "")
                {

                    br++;

                }
            }

            if (br > 0)
            {
                UsernameWarning.InnerText = "Korisničko ime ili email već postoje.Sva polja moraju biti popunjena.";
            }
            else
            {
                User user = new User();
                user.Email = email;
                user.Username = username;
                user.Password = password;
                user.Permission = "user";
                this.userBussiness.insertUser(user);
                UsernameWarning.Style.Add("color", "green");
                UsernameWarning.InnerText = "Uspešno ste se registrovali." + Environment.NewLine + "Bićete preusmereni na početnu stranicu odakle se možete prijaviti.";
                toRedirect = true;
            }


            if (toRedirect == true)
            {

                Response.AddHeader("REFRESH", "5,URL=./Default.aspx");
            }

        }

    }
}








