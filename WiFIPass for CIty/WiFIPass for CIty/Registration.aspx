﻿<%@ Page Title="Registracija" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Registration.aspx.cs" Inherits="WiFIPass_for_CIty.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    
    <style>
        
        
        #username, #password, #email{
            background-color:white;
            height:50px;
            color:black;
            margin:auto;
        }
        #register{
            width:250px;
        }
        .col{
            align-content:center;
        }
        a{
            color:white;
            
        }
        a:hover{
            color:grey;
        }
       #message-box{
           width:250px;
           margin:auto;

       }
        
    </style>
    
        <div id="showcase" class="d-flex justify-content-center align-items-center text-white"
        style="background-image: url(./images/pozadina.jpg);background-position: center bottom; min-height:900px;">
         
        <div class="container text-center">
            <h1><b> WiFi Pass for City</b></h1><br />
            <h3><b>Dobrodošli na stranicu za registraciju.</b></h3><br /><br />
            <h5>Registracijom stičete pravo da dodajete nove WIFI mreže. Proces registracije je jednostavan. Potrebno je da popunite sva polja i kliknete na dugme "Potvrdi registraciju", nakon čega ćete moći da se ulogujete na vaš nalog.</></h5><br /><br />
        
           

       <div class="d-flex h-100 text-center">
               
           <div class="m-auto">
                <h2><b>Registracija</b></h2><br /><br />

       <form>
     
              <div class="col">
                  <label class="sr-only" for="Username">Username</label>
                    <div class="input-group">
                         <div class="input-group-prepend">
                             <div class="input-group-text"><i class="fa fa-user"></i></div>
                        </div>
                 <input type="text" class="form-control" id="UsernameReg" placeholder="Korisničko ime" runat="server"><br />
                 
              </div>
                  
         </div><br />
            <div class="col">
                  <label class="sr-only" for="Password">Password</label>
                    <div class="input-group">
                         <div class="input-group-prepend">
                             <div class="input-group-text"><i class="fa fa-unlock"></i></div>
                        </div>
                 <input  type="password" class="form-control" id="PasswordReg" placeholder="Lozinka" runat="server">
              </div>
                
         </div><br />
             <div class="col">
                  <label class="sr-only" for="Email">Email</label>
                    <div class="input-group">
                         <div class="input-group-prepend">
                             <div class="input-group-text"><i class="fa fa-at"></i></div>
                        </div>
                 <input type="email" class="form-control" id="EmailReg" placeholder="Email" runat="server">
              </div><br />
                
                
                 <!-- ASP.NET button. When the user press the button the data about new user will be sent to database-->
                 <asp:Button ID="Registration" runat="server" Text="Potvrdi registraciju" class="btn btn-lg btn-success" OnClick ="Registration_Click" /><br /><br />
                 <a href="./Default.aspx"><u>Vrati se na početnu stranu</u></a>
                 <br />
                 <br />
                 
         </div>
           
    </form>
  </div>
                    
    </div>
             <div id="message-box">
                    <h6 id="UsernameWarning" runat="server" style="color:red; background-color:white;"></h6>
           </div>
</div>
</asp:Content>
 