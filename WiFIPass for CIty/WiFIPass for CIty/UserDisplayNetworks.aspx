﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="UserDisplayNetworks.aspx.cs" Inherits="WiFIPass_for_CIty.UserDisplayNetworks" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <style>
        .card-deck{
            float:left;
            padding-top:30px;
            text-align:center;
        }
        
        .listCity{
            font-size:25px;
            border:2px solid black;
            color:white;
            color:black;
            border-radius: 5px 5px;
            
        }
        .message{
            color:white;
            border-radius:5px 5px;
            padding:10px;
        }
        
    </style>
   <div class="container text-center" runat="server" id="container">
       <br /><br />  
       <h2>Prikaz postojećih WiFi mreža</h2>
        <br /><br />
       <a href="./UserLoggedIn.aspx" style="color:white;"><h4><u>Vrati se na početnu stranu</u></a></h4>
       <div class="container text-light text-center" >
           <h6>Iz padajuće liste izaberite grad za koji želite da vidite dostupne mreže.</h6>
           <asp:DropDownList id="listCity" runat="server" CssClass="listCity"/>
           <br/><br />
           <asp:Button id="search" runat="server" Text="Pretraži"  CssClass="btn btn-secondary btn-lg" OnClick="search_Click"/>
           <br /><br />
           <asp:Label id="message" runat="server" CssClass="message"></asp:Label>
           
           
          
       </div>
        
   </div>
    

    </asp:Content>
