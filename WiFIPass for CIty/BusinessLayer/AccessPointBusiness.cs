﻿using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class AccessPointBusiness
    {
        AccessPointRepository accessPointRepository;

        public AccessPointBusiness() {
            this.accessPointRepository = new AccessPointRepository();
        }

        public bool insertAccessPoint(AccessPoint ap)
        {
            if (this.accessPointRepository.InsertAccessPoint(ap) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<AccessPoint> GetAllAccessPoints()
        {
            return this.accessPointRepository.GetAllAccessPoints();
        }

        public SqlDataReader DisplayAccessPoint(String city) {
            return this.accessPointRepository.DisplayAccessPoint(city);
        }


        public bool UpdateAcessPoint(AccessPoint ap)
        {
            int result = 0;
            if (ap != null)
            {
                result = this.accessPointRepository.UpdateAccessPoint(ap);
            }
            if (result > 0)
            {
                return true;
            }
            return false;
        }

        public bool DeleteAccessPoint(int id)
        {
            int result = this.accessPointRepository.DeleteAccessPointt(id);
            if (result > 0)
            {
                return true;
            }
            return false;
        }

    }
}
