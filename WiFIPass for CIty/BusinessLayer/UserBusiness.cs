﻿using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class UserBusiness
    {
        private UserRepository userRepository;

        public UserBusiness() {
            this.userRepository = new UserRepository();
        }

        public bool insertUser(User u) {
            if (this.userRepository.InsertUser(u) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

            public List<User> GetAllUsers(){

            return this.userRepository.GetAllUsers();
        }
         
    }
}
