﻿using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class LocationBusiness
    {
        private LocationRepository locationRepository;

        public LocationBusiness() {
            this.locationRepository = new LocationRepository();
        }

        public bool insertLocation(Location l)
        {
            if (this.locationRepository.InsertLocation(l) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<Location> GetAllLocations() {
            return this.locationRepository.GetAllLocations();
        }


        public bool UpdateLocation(Location l)
        {
            int result = 0;
            if (l != null)
            {
                result = this.locationRepository.UpdateLocation(l);
            }
            if (result > 0)
            {
                return true;
            }
            return false;
        }

        public bool DeleteLocation(int id)
        {
            int result = this.locationRepository.DeleteLocations(id);
            if (result > 0)
            {
                return true;
            }
            return false;
        }
    }
}
