﻿using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class CompanyBusiness
    {
        CompanyRepository companyRepository;

        public CompanyBusiness() {
            this.companyRepository = new CompanyRepository();
        }

        public bool insertCompany(Company c)
        {
            if (this.companyRepository.InsertCompany(c) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<Company> GetAllCompanies()
        {

            return this.companyRepository.GetAllCompanies();
        }

        public bool UpdateCompany(Company c)
        {
            int result = 0;
            if (c != null)
            {
                result = this.companyRepository.UpdateCompany(c);
            }
            if (result > 0)
            {
                return true;
            }
            return false;
        }

        public bool DeleteCompany(int id)
        {
            int result = this.companyRepository.DeleteCompany(id);
            if (result > 0)
            {
                return true;
            }
            return false;
        }
    }
}
