﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class UserRepository
    {
        //Stavite ispod connection string vase baze da bi ste se povezali sa bazom
        private string connectionString = "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=WiFiDB;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        public int InsertUser(User u)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = "INSERT INTO Users VALUES("+string.Format("'{0}','{1}','{2}','{3}'",u.Email,u.Username,u.Password,u.Permission)+")";

                return sqlCommand.ExecuteNonQuery();
            }
        }

        public List<User> GetAllUsers()
        {
            using(SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = "SELECT * FROM Users";

                List<User> listOfUsers = new List<User>();
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                while (sqlDataReader.Read())
                {
                    User u = new User();
                    u.Id = sqlDataReader.GetInt32(0);
                    u.Email = sqlDataReader.GetString(1);
                    u.Username = sqlDataReader.GetString(2);
                    u.Password = sqlDataReader.GetString(3);
                    u.Permission = sqlDataReader.GetString(4);

                    listOfUsers.Add(u);
                }

                return listOfUsers;

            }
        }
    }
}
