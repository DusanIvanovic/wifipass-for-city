﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class LocationRepository
    {
        private string connectionString = "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=WiFiDB;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        public int InsertLocation(Location l)
        {
            using(SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = "INSERT INTO Locations VALUES(" + string.Format("'{0}','{1}','{2}'", l.Address,l.City, l.Company_Id) + ")";

                return sqlCommand.ExecuteNonQuery();
            }
        }

        public List<Location> GetAllLocations()
        {
            using(SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = "SELECT * FROM Locations";

                List<Location> listOfLocations = new List<Location>();
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                while (sqlDataReader.Read())
                {
                    Location l = new Location();
                    l.Id = sqlDataReader.GetInt32(0);
                    l.Address = sqlDataReader.GetString(1);
                    l.City = sqlDataReader.GetString(2);
                    l.Company_Id = sqlDataReader.GetInt32(3);

                    listOfLocations.Add(l);
                }

                return listOfLocations;
            }
        }

        public int UpdateLocation(Location l)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = "UPDATE Locations SET Address='" + l.Address + "',City='" + l.City + "'WHERE Id=" + l.Id;

                return sqlCommand.ExecuteNonQuery();
            }
        }

        public int DeleteLocations(int id)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = "DELETE FROM Locations WHERE Id=" + id;

                return sqlCommand.ExecuteNonQuery();
            }
        }


    }
}
