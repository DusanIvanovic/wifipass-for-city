﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class CompanyRepository
    {
        private string connectionString = "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=WiFiDB;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        public int InsertCompany(Company c)
        {
            using(SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = "INSERT INTO Companies VALUES(" + string.Format("'{0}','{1}','{2}'", c.Name, c.Contact, c.Type) + ")";

                return sqlCommand.ExecuteNonQuery();
            }
        }

        public List<Company> GetAllCompanies()
        {
            using(SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = "SELECT * FROM Companies";
                List<Company> listOfUsers = new List<Company>();
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    Company c = new Company();
                    c.Id = sqlDataReader.GetInt32(0);
                    c.Name = sqlDataReader.GetString(1);
                    c.Contact = sqlDataReader.GetString(2);
                    c.Type = sqlDataReader.GetString(3);

                    listOfUsers.Add(c);
                }

                return listOfUsers;
            }
        }

        public int UpdateCompany(Company c)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = "UPDATE Companies SET Name='" + c.Name + "',Contact='" + c.Contact + "',Type='" + c.Type + "'  WHERE Id=  " + c.Id;

                return sqlCommand.ExecuteNonQuery();
            }
        }

        public int DeleteCompany(int id)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = "DELETE FROM Companies WHERE Id=" + id;

                return sqlCommand.ExecuteNonQuery();
            }
        }


    }
}
