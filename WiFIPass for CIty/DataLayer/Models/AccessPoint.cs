﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Models
{
    public class AccessPoint
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public int Location_Id { get; set; }
    }
}
