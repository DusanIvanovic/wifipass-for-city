﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class AccessPointRepository
    {
        private string connectionString = "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=WiFiDB;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        public int InsertAccessPoint(AccessPoint a)
        {
            using(SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = "INSERT INTO AccessPoints VALUES(" + string.Format("'{0}','{1}','{2}'", a.Name, a.Password, a.Location_Id) + ")";

                return sqlCommand.ExecuteNonQuery();
            }
        }

        public List<AccessPoint> GetAllAccessPoints()
        {
            using(SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = "SELECT * FROM AccessPoints";

                List<AccessPoint> listOfAccessPoints = new List<AccessPoint>();
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                while (sqlDataReader.Read())
                {
                    AccessPoint a = new AccessPoint();
                    a.Id = sqlDataReader.GetInt32(0);
                    a.Name = sqlDataReader.GetString(1);
                    a.Password = sqlDataReader.GetString(2);
                    a.Location_Id = sqlDataReader.GetInt32(3);

                    listOfAccessPoints.Add(a);
                }

                return listOfAccessPoints;
            }
        }

        public int UpdateAccessPoint(AccessPoint a)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = "UPDATE AccessPoints SET Name='"+ a.Name + "',Password='" + a.Password + "' WHERE Id=" + a.Id;

                return sqlCommand.ExecuteNonQuery();
            }
        }

        public int DeleteAccessPointt(int id)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = "DELETE FROM AccessPoints WHERE Id=" + id;

                return sqlCommand.ExecuteNonQuery();
            }
        }

        public SqlDataReader DisplayAccessPoint(String city)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);

            sqlConnection.Open();
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.Connection = sqlConnection;
            sqlCommand.CommandText = "SELECT cmp.Name, cmp.Contact, loc.Address, loc.City, ap.Name AS NetworkName, ap.Password , cmp.Id AS CompanyId, loc.Id AS LocationId, ap.Id AS AccessPointId FROM Companies cmp, Locations loc, AccessPoints ap WHERE loc.Company_Id = cmp.Id AND ap.Location_Id = loc.Id AND City= '" +city +"'";
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

            return sqlDataReader;
        }


    }
}
