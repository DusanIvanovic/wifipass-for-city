﻿using System;
using BusinessLayer;
using DataLayer.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests
{
    [TestClass]
    public class UnitTestAccessPoints
    {
        Company c = new Company();
        Location l = new Location();
        AccessPoint a = new AccessPoint();
        CompanyBusiness CB = new CompanyBusiness();
        LocationBusiness LB = new LocationBusiness();
        AccessPointBusiness AB = new AccessPointBusiness();

        [TestInitialize]
        public void Init()
        {
            c.Name = "TestName";
            c.Contact = "TestContact";
            c.Type = "TestType";
            CB.insertCompany(c);
            l.Address = "TestAddress";
            l.City = "TestCity";
            foreach(Company company in CB.GetAllCompanies())
            {
                if(c.Name == company.Name)
                {
                    c.Id = company.Id;
                }
            }
            l.Company_Id = c.Id;
            LB.insertLocation(l);
            a.Name = "TestName";
            a.Password = "TestPassword";
            foreach(Location location in LB.GetAllLocations())
            {
                if(l.Address==location.Address && l.City == location.City)
                {
                    l.Id = location.Id;
                }
            }
            a.Location_Id = l.Id;
            AB.insertAccessPoint(a);

            foreach(AccessPoint ap in AB.GetAllAccessPoints())
            {
                if(a.Name==ap.Name && a.Password == ap.Password)
                {
                    a.Id = ap.Id;
                }
            }
        }


        [TestMethod]
        public void TestInsertAccessPoint()
        {
            if(a.Id==' ')
            {
                Assert.Fail("Access point nije unet u bazu.");
            }
        }

        [TestMethod]
        public void TestUpdateAccessPoint()
        {
            a.Name = "TestName2";
            a.Password = "TestPassword2";
            AB.UpdateAcessPoint(a);
            foreach(AccessPoint ap in AB.GetAllAccessPoints())
            {
                if (a.Id == ap.Id)
                {
                    Assert.AreEqual(a.Name, ap.Name);
                    Assert.AreEqual(a.Password, ap.Password);
                }
            }
        }

        [TestMethod]
        public void TestDeleteLocation()
        {
            AB.DeleteAccessPoint(a.Id);
            foreach(AccessPoint ap in AB.GetAllAccessPoints())
            {
                if (a.Id == ap.Id)
                {
                    Assert.Fail("Access point nije obrisan iz baze.");
                }
            }
        }

        [TestCleanup]
        public void Cleanup()
        {
            AB.DeleteAccessPoint(a.Id);
            LB.DeleteLocation(l.Id);
            CB.DeleteCompany(c.Id);
        }
        
    }
}
