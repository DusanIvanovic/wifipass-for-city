﻿using System;
using BusinessLayer;
using DataLayer.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests
{

    [TestClass]
    public class UnitTestUser
    {
        User u = new User();
        UserBusiness UB = new UserBusiness();

        [TestInitialize]
        public void Init() {
            u.Email = "TestEmail";
            u.Username = "TestUsername";
            u.Password = "TestPassword";
            u.Permission = "user";
            UB.insertUser(u);

            foreach (User user in UB.GetAllUsers()){
                if (user.Username == u.Username)
                {
                    u.Id = user.Id;
                }
            }
        }

        [TestMethod]
        public void TestInsertUser()
        {
            if (u.Id == ' ')
            {
                Assert.Fail("Korisnik nije ubacena u bazu.");
            }
        }

    }
}
