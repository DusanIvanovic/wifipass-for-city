﻿using System;
using BusinessLayer;
using DataLayer.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests
{
    [TestClass]
    public class UnitTestCompany
    {
        Company c = new Company();
        CompanyBusiness CB = new CompanyBusiness();

        [TestInitialize]
            public void Init() {
            c.Name = "TestName";
            c.Contact = "TestContact";
            c.Type = "TestType";
            CB.insertCompany(c);

            foreach (Company company in CB.GetAllCompanies()) {
                if (company.Name == c.Name) {
                    c.Id = company.Id;
                }
            }
        }

        [TestMethod]
        public void TestInsertCompany()
        {
            if (c.Id == ' ') {
                Assert.Fail("Kompanija nije ubacena u bazu.");
            }
        }

        [TestMethod]

        public void TestUpdateComapny() {
            c.Name = "TestName2";
            c.Contact = "TestContact2";
            CB.UpdateCompany(c);
            foreach (Company company in CB.GetAllCompanies()) {
                if (company.Id == c.Id) {
                    Assert.AreEqual("TestName2", company.Name);
                    Assert.AreEqual("TestContact2", company.Contact);
                }
            }
        }

        [TestMethod]

        public void TestDeleteCompany() {
            CB.DeleteCompany(c.Id);

            foreach (Company company in CB.GetAllCompanies()) {
                if (company.Id == c.Id) {
                    Assert.Fail("Kompanija nije obrisana iz baze.");
                }
            }
        }

        [TestCleanup]

        public void Cleanup() {
            CB.DeleteCompany(c.Id);
        }
    }
}
