﻿using System;
using BusinessLayer;
using DataLayer.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests
{
    [TestClass]
    public class UnitTestLocation
    {
        Location l = new Location();
        Company c = new Company();
        LocationBusiness LB = new LocationBusiness();
        CompanyBusiness CB = new CompanyBusiness();

        [TestInitialize]
        public void Init()
        {
            c.Name = "TestName";
            c.Contact = "TestContact";
            c.Type = "TestType";
            CB.insertCompany(c);
            l.Address = "TestAddress";
            l.City = "TestCity";
            foreach(Company company in CB.GetAllCompanies())
            {
                if(c.Name == company.Name)
                {
                    c.Id = company.Id;
                }
            }
            l.Company_Id = c.Id;
            LB.insertLocation(l);

            foreach(Location location in LB.GetAllLocations())
            {
                if(l.Address == location.Address && l.City == location.City)
                {
                    l.Id = location.Id;
                }
            }
        }

        [TestMethod]
        public void TestInsertLocation()
        {
            if(l.Id ==' ')
            {
                Assert.Fail("Lokacija nije uneta u bazu.");
            }
        }

        [TestMethod]
        public void TestUpdateLocation()
        {
            l.Address = "TestAddress2";
            l.City = "TestCity2";
            LB.UpdateLocation(l);
            foreach(Location location in LB.GetAllLocations())
            {
                if(l.Id == location.Id)
                {
                    Assert.AreEqual(l.Address, location.Address);
                    Assert.AreEqual(l.City, location.City);
                }
            }
        }

        [TestMethod]
        public void TestDeleteLocation()
        {
            LB.DeleteLocation(l.Id);

            foreach(Location location in LB.GetAllLocations())
            {
                if(l.Id == location.Id)
                {
                    Assert.Fail("Lokacija nije obrisana iz baze.");
                }
            }
        }

        [TestCleanup]
        public void Cleaup()
        {
            LB.DeleteLocation(l.Id);
            CB.DeleteCompany(c.Id);
        }
    }
}
